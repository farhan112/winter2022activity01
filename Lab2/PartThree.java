import java.util.Scanner;
public class PartThree{
	
	public static void main(String[] args){
	
	Scanner scan = new Scanner (System.in);
	
	//Input Values
	System.out.println("Enter the length of the square to get its area.");
	int squareLength = scan.nextInt();
	
	System.out.println("Enter the length of the rectangle.");
	int rectangleLength = scan.nextInt();
	
	System.out.println("Enter the width of the rectangle to get its area.");
	int rectangleWidth = scan.nextInt();
	
	int areaSquare = AreaComputations.areaSquare(squareLength);
	System.out.println("The area of the square is " + areaSquare + ".");
	
	AreaComputations calculateArea = new AreaComputations();
	int areaRectangle = calculateArea.areaRectangle(rectangleLength, rectangleWidth);
	System.out.println("The area of the rectangle is " + areaRectangle + ".");
	
	}
}